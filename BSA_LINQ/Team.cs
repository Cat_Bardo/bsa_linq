﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQ
{
    public class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        public Team (int id)
        {
            this.Id = id;
        }
    }
}
