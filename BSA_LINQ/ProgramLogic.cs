﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BSA_LINQ
{
    public class ProgramLogic
    {
        static string path = "https://bsa20.azurewebsites.net/api/";
        static HttpClient httpClient = new HttpClient();
        public static List<Project> projects;

        public static void Main() { }
        public static async Task<List<Project>> GetProjectStruct()
        {
            var users = await GetUsersAsync();
            var tasks = await GetTasksAsync();
            var projects = await GetProjectsAsync();
            var teams = await GetTeamsAsync();

            return projects
                .GroupJoin(tasks, proj => proj.Id, task => task.ProjectId, (proj, task) => new { proj, tasks = task.Select(t => t).ToList() })
                .Join(teams, x => x.proj.TeamId, t => t.Id, (x, team) => new { x, team })
                .Join(users, x => x.x.proj.AuthorId, u => u.Id, (x, author) => new { x, author })
                .Select(x
                => new Project
                {
                    Id = x.x.x.proj.Id,
                    Name = x.x.x.proj.Name,
                    Description = x.x.x.proj.Description,
                    CreatedAt = x.x.x.proj.CreatedAt,
                    Deadline = x.x.x.proj.Deadline,
                    AuthorId = x.x.x.proj.AuthorId,
                    TeamId = x.x.x.proj.TeamId,
                    Author = x.author,
                    Team = x.x.team,
                    Tasks = x.x.x.tasks.Join(users, task => task.PerformerId, user => user.Id, (task, performer) =>
                           new ProjectTask
                           {
                               Performer = performer,
                               State = task.State,
                               CreatedAt = task.CreatedAt,
                               FinishedAt = task.FinishedAt,
                               PerformerId = task.PerformerId,
                               ProjectId = task.ProjectId,
                               Description = task.Description,
                               Name = task.Name,
                               Id = task.Id
                           }).ToList()
                }).ToList();
        }

        //First query from hw tasks
        public static Dictionary<Project, int> TaskCount(int id)
        {
            return projects.SelectMany(x => x.Tasks)
                .Where(x => x.PerformerId == id)
                .GroupBy(x => x.ProjectId)
                .ToDictionary(t => projects.First(a => a.Id == t.Key), t => t.Count());
        }

        //Second query from hw tasks
        public static List<ProjectTask> Tasks(int id)
        {
            return projects.SelectMany(x => x.Tasks)
                .Where(t => t.PerformerId == id && t.Name.Length < 45)
                .ToList();
        }

        //Third query from hw tasks
        public static List<Tuple<int, string>> FinishedTasks(int id)
        {
            return projects.SelectMany(x => x.Tasks)
                .Where(x => x.PerformerId == id)
                .Where(x => x.FinishedAt.Year == 2020)
                .Select(a => new { a.Id, a.Name })
                .Select(x => new Tuple<int, string>(x.Id, x.Name)).ToList();
        }

        //Fourth query from hw tasks
        public static List<Tuple<int, string, List<User>>> TeamMembersOverTenYearsOld()
        {

            return projects.Select(x => x.Team).Distinct()
                .GroupJoin(projects.SelectMany(x => x.Tasks).Select(x => x.Performer)
                .Where(x => 2020 - x.Birthday.Year > 10)
                .OrderByDescending(x => x.RegisteredAt).Distinct(), t => t.Id, x => x.TeamId, (team, user) => new { team, users = user.Select(u => u).ToList() })
                .Select(x => new Tuple<int, string, List<User>>(x.team.Id, x.team.Name, x.users))
                .ToList();
        }

        //Fifth query from hw tasks
        public static List<Tuple<User, ProjectTask>> AscendingUsersFirst_NameAndDescendingTaskName()
        {
            return projects.SelectMany(x => x.Tasks)
                .Select(x => new { task = x, x.Performer })
                .OrderBy(u => u.Performer.FirstName)
                .ThenByDescending(t => t.task.Name.Length)
                .Select(x => new Tuple<User, ProjectTask>(x.Performer, x.task))
                .ToList();
        }

        //Sixth query from hw tasks
        public static Tuple<User, Project, int, int, ProjectTask> GetStructUser(int id)
        {
            var user = projects.SelectMany(x => x.Tasks).First(x => x.Performer.Id == id).Performer;
            var lastProject = projects.Where(x => x.Team.Id == projects
            .SelectMany(x => x.Tasks).First(x => x.Performer.Id == id).ProjectId)
            .First(x => x.CreatedAt == projects.Max(x => x.CreatedAt));
            var tasksCount = lastProject.Tasks.Count();
            var finishedAndCanceledTasksCount = projects.SelectMany(x => x.Tasks)
                .Where(x => x.Performer.Id == id)
                .Where(x => x.State == 2 || x.State == 3).Count();
            var longestTask = projects.SelectMany(x => x.Tasks)
                .Where(x => x.Performer.Id == id).First(x => x.FinishedAt - x.CreatedAt == projects.SelectMany(x => x.Tasks)
                .Where(x => x.Performer.Id == id).Max(x => x.FinishedAt - x.CreatedAt));

            var result = new Tuple<User, Project, int, int, ProjectTask>
                (user, lastProject, tasksCount, finishedAndCanceledTasksCount, longestTask);
            return result;
        }

        //Seventh query from hw tasks


        // Methods for requesting a server
        static async Task<Project> GetProjectAsync(int id)
        {
            return await (await httpClient.GetAsync(path + "Projects/" + id)).
                EnsureSuccessStatusCode().Content.ReadAsAsync<Project>();
        }

        static async Task<List<Project>> GetProjectsAsync()
        {
            return await (await httpClient.GetAsync(path + "Projects")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<Project>>();
        }
        static async Task<ProjectTask> GetTaskAsync(int id)
        {
            return await (await httpClient.GetAsync(path + "Tasks/" + id)).
                EnsureSuccessStatusCode().Content.ReadAsAsync<ProjectTask>();
        }

        static async Task<List<ProjectTask>> GetTasksAsync()
        {
            return await (await httpClient.GetAsync(path + "Tasks")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<ProjectTask>>();
        }

        static async Task<List<TaskStates>> GetTaskStatesAsync()
        {
            return await (await httpClient.GetAsync(path + "TasksStates")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<TaskStates>>();
        }

        static async Task<Team> GetTeamAsync(int id)
        {
            return await (await httpClient.GetAsync(path + "Teams/" + id)).
                EnsureSuccessStatusCode().Content.ReadAsAsync<Team>();
        }

        static async Task<List<Team>> GetTeamsAsync()
        {
            return await (await httpClient.GetAsync(path + "Teams")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<Team>>();
        }
        static async Task<User> GetUserAsync(int id)
        {
            return await (await httpClient.GetAsync(path + "Users/" + id)).
                EnsureSuccessStatusCode().Content.ReadAsAsync<User>();
        }

        static async Task<List<User>> GetUsersAsync()
        {
            return await (await httpClient.GetAsync(path + "Users")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<User>>();
        }
    }
}
