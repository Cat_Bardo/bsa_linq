﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BSA_Kolobov_LINQ
{
    public class ProgramLogic
    {
        static string path = "https://bsa20.azurewebsites.net/api/";
        static HttpClient httpClient = new HttpClient();
        public static List<Project> projects;
        public static async Task<List<Project>> GetProjectStruct()
        {
            var users = await GetUsersAsync();
            var tasks = await GetTasksAsync();
            var projects = await GetProjectsAsync();
            var teams = await GetTeamsAsync();

            return projects
                .GroupJoin(tasks, proj => proj.Id, task => task.ProjectId, (proj, task) => new { proj, tasks = task.Select(t => t).ToList() })
                .Join(teams, x => x.proj.TeamId, t => t.Id, (x, team) => new { x, team })
                .Join(users, x => x.x.proj.AuthorId, u => u.Id, (x, author) => new { x, author })
                .Select(x
                => new Project
                {
                    Id = x.x.x.proj.Id,
                    Name = x.x.x.proj.Name,
                    Description = x.x.x.proj.Description,
                    CreatedAt = x.x.x.proj.CreatedAt,
                    Deadline = x.x.x.proj.Deadline,
                    AuthorId = x.x.x.proj.AuthorId,
                    TeamId = x.x.x.proj.TeamId,
                    Author = x.author,
                    Team = x.x.team,
                    Tasks = x.x.x.tasks.Join(users, task => task.PerformerId, user => user.Id, (task, performer) =>
                           new ProjectTask
                           {
                               Performer = performer,
                               State = task.State,
                               CreatedAt = task.CreatedAt,
                               FinishedAt = task.FinishedAt,
                               PerformerId = task.PerformerId,
                               ProjectId = task.ProjectId,
                               Description = task.Description,
                               Name = task.Name,
                               Id = task.Id
                           }).ToList()
                }).ToList();
        }

        //First query from hw tasks
        public static Dictionary<Project, int> TaskCount(int id)
        {
            if (projects.Any(x => x.AuthorId == id))
            {
                //return projects.ToDictionary(x => projects.First(k => k.AuthorId == id), t => t.Tasks.Count());
                return projects.Where(x => x.AuthorId == id).ToDictionary(x => x, x => x.Tasks.Count());
            }
            else
            {
                throw new ArgumentException("Користувач не має проектiв");
            }
        }

        //Second query from hw tasks
        public static List<ProjectTask> Tasks(int id)
        {
            return projects.SelectMany(x => x.Tasks)
                .Where(t => t.PerformerId == id && t.Name.Length < 45)
                .ToList();
        }

        //Third query from hw tasks
        public static List<Tuple<int, string>> FinishedTasks(int id)
        {
            return projects.SelectMany(x => x.Tasks).Where(x => x.PerformerId == id).Where(x => x.FinishedAt.Year == 2020 && x.State == 2)
                .Select(a => new { a.Id, a.Name })
                .Select(x => new Tuple<int, string>(x.Id, x.Name))
                .ToList();
        }


        //Fourth query from hw tasks
        public static List<Tuple<int, string, List<User>>> TeamMembersOverTenYearsOld()
        {

            return projects.Select(x => x.Team).Distinct()
                .GroupJoin(projects.SelectMany(x => x.Tasks).Select(x => x.Performer)
                .Where(x => 2020 - x.Birthday.Year > 10)
                .OrderByDescending(x => x.RegisteredAt).Distinct(), t => t.Id, x => x.TeamId, (team, user) => new { team, users = user.Select(u => u).ToList() })
                .Select(x => new Tuple<int, string, List<User>>(x.team.Id, x.team.Name, x.users))
                .ToList();
        }

        //Fifth query from hw tasks
        public static List<Tuple<User, ProjectTask>> AscendingUsersFirst_NameAndDescendingTaskName()
        {
            return projects.SelectMany(x => x.Tasks)
                .Select(x => new { task = x, x.Performer })
                .OrderBy(u => u.Performer.FirstName)
                .ThenByDescending(t => t.task.Name.Length)
                .Select(x => new Tuple<User, ProjectTask>(x.Performer, x.task))
                .ToList();
        }

        //Sixth query from hw tasks
        public static Tuple<User, Project, int, int, ProjectTask> GetStructUser(int id)
        {
            Project lastProject = new Project();
            int tasksCount = 0, finishedAndCanceledTasksCount = 0;
            ProjectTask longestTask = new ProjectTask();
            User user = projects.SelectMany(x => x.Tasks).Select(x => x.Performer)
                .Union(projects.Select(p => p.Author)).First(x => x.Id == id);
            if (projects.SelectMany(p => p.Tasks).Any(t => t.PerformerId == user.Id))
            {
                finishedAndCanceledTasksCount = projects.SelectMany(x => x.Tasks)
                   .Where(x => x.Performer.Id == id)
                   .Where(x => x.State == 2 || x.State == 3).Count();
                longestTask = projects.SelectMany(x => x.Tasks)
                   .Where(x => x.Performer.Id == id)
                   .First(x => x.FinishedAt - x.CreatedAt == projects.SelectMany(x => x.Tasks)
                   .Where(x => x.Performer.Id == id).Max(x => x.FinishedAt - x.CreatedAt));
            }
            if (projects.Any(x => x.AuthorId == id))
            {
                lastProject = projects.Where(x => x.AuthorId == id)
                        .First(x => x.CreatedAt == projects.Where(x => x.AuthorId == user.Id).Max(x => x.CreatedAt));
                tasksCount = lastProject.Tasks.Count();
            }
            return new Tuple<User, Project, int, int, ProjectTask>
                    (user, lastProject, tasksCount, finishedAndCanceledTasksCount, longestTask);
        }

        //Seventh query from hw tasks
        public static List<Tuple<Project, ProjectTask, ProjectTask, int>> GetStructProject()
        {
            var result2 = projects.Where(x => x.Tasks.Count() != 0).Select(proj => new Tuple<Project, ProjectTask, ProjectTask, int>
              (
                  proj,
                  proj.Tasks
                          .First(x => x.Description.Length == proj.Tasks
                          .Max(x => x.Description.Length)),
                  proj.Tasks
                         .First(x => x.Name.Length == proj.Tasks
                         .Min(x => x.Name.Length)),
                  projects.SelectMany(x => x.Tasks).Select(x => x.Performer).Union(projects.Select(p => p.Author))
                         .Where(u => u.TeamId == projects
                         .First(x => (x.Description.Length > 20 || x.Tasks.Count < 3) && x.Id == proj.Id).TeamId).Count()))
                         .ToList();
            return result2;
        }

        // Methods for requesting a server
        static async Task<List<Project>> GetProjectsAsync()
        {
            return await (await httpClient.GetAsync(path + "Projects")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<Project>>();
        }

        static async Task<List<ProjectTask>> GetTasksAsync()
        {
            return await (await httpClient.GetAsync(path + "Tasks")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<ProjectTask>>();
        }

        static async Task<List<Team>> GetTeamsAsync()
        {
            return await (await httpClient.GetAsync(path + "Teams")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<Team>>();
        }

        static async Task<List<User>> GetUsersAsync()
        {
            return await (await httpClient.GetAsync(path + "Users")).
               EnsureSuccessStatusCode().Content.ReadAsAsync<List<User>>();
        }
    }
}
